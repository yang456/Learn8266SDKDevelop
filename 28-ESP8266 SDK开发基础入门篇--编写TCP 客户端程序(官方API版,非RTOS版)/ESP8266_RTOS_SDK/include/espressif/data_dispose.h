/*
 * data_dispose.h
 *
 *  Created on: 2019年6月28日
 *      Author: yang
 */

#ifndef INCLUDE_ESPRESSIF_DATA_DISPOSE_H_
#define INCLUDE_ESPRESSIF_DATA_DISPOSE_H_

#ifdef __cplusplus
extern "C" { //使用C编译器编译
#endif



typedef union Resolve//解析数据
{
	char  Data_Table[4];//16进制四字节表示形式
	float DataF;//浮点型
	long DataInt;//有符号整形
	unsigned long DatauInt;//无符号整形
}ResolveData;



#ifdef __cplusplus
}
#endif

#endif /* INCLUDE_ESPRESSIF_DATA_DISPOSE_H_ */
