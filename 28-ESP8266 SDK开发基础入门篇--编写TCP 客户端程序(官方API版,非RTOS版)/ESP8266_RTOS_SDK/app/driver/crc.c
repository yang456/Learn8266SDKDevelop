/*
 * crc.c
 *
 *  Created on: 2019年8月9日
 *      Author: yang
 */
#include "esp_common.h"
/**
* @brief  计算CRC
* @param  *modbusdata:数据指针
* @param  length:需要计算的CRC的数据长度
* @param
* @retval 计算的CRC
* @example
**/
int crc16_modbus(u8 *modbusdata, int length)
{
	int i, j;
	int crc = 0xffff;//有的用0有的用0xffff
	for (i = 0; i < length; i++)
	{
			crc ^= modbusdata[i];
			for (j = 0; j < 8; j++)
			{
					if ((crc & 0x01) == 1)
					{
							crc = (crc >> 1) ^ 0xa001;
					}
					else
					{
							crc >>= 1;
					}
			}
	}

	return crc;
}

/**
* @brief  判断CRC的校验是否正确
* @param  *modbusdata:要判断的数据指针
* @param  length:需要计算的CRC的数据长度
* @param
* @retval 1 OK·  0 err
* @example
**/
int crc16_flage(u8 *modbusdata, int length)
{
	int Receive_CRC=0,calculation=0;

	Receive_CRC = crc16_modbus(modbusdata, length);//用自带的函数计算数据的CRC
	calculation = modbusdata[length];//获得接收的CRC的高位
	calculation <<= 8;//获得接收的CRC的低位
	calculation += modbusdata[length+1];//高低位组合
	if(calculation != Receive_CRC)//看看CRC是否相等
	{
		return 0;
	}
	return 1;
}
