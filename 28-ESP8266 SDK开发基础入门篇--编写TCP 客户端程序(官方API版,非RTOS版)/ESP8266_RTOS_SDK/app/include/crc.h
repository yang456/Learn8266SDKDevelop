/*
 * crc.h
 *
 *  Created on: 2019��8��9��
 *      Author: yang
 */

#ifndef APP_INCLUDE_CRC_H_
#define APP_INCLUDE_CRC_H_

int crc16_modbus(u8 *modbusdata, int length);
int crc16_flage(u8 *modbusdata, int length);

#endif /* APP_INCLUDE_CRC_H_ */
