/*
 * ESPRSSIF MIT License
 *
 * Copyright (c) 2015 <ESPRESSIF SYSTEMS (SHANGHAI) PTE LTD>
 *
 * Permission is hereby granted for use on ESPRESSIF SYSTEMS ESP8266 only, in which case,
 * it is free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include "esp_common.h"
#include "gpio.h"
#include "uart.h"
#include "esp_timer.h"
#include "hw_timer.h"
#include "pwm.h"
#include  "data_dispose.h"


extern u8  Usart1ReadBuff[Usart1ReadLen];//接收数据的数组
extern u32 Usart1ReadCnt;//串口1接收到的数据个数
extern u32 Usart1ReadCntCopy;//串口1接收到的数据个数拷贝
extern u8  Usart1ReadFlage;//串口1接收到一条完整数据



char testdata[4] = {0x43,0x5C,0x80,0x00};//模拟发过来的数据

ResolveData ResolveDataTest;


os_timer_t os_timer_one;//定义一个全局的定时器结构体变量


uint32 pin_info_list[1][3]={PERIPHS_IO_MUX_GPIO5_U,FUNC_GPIO5,5};//配置GPIO5作为PWM输出
int duty[1]={0};//高电平时间是0us


/******************************************************************************
 * FunctionName : user_rf_cal_sector_set
 * Description  : SDK just reversed 4 sectors, used for rf init data and paramters.
 *                We add this function to force users to set rf cal sector, since
 *                we don't know which sector is free in user's application.
 *                sector map for last several sectors : ABCCC
 *                A : rf cal
 *                B : rf init data
 *                C : sdk parameters
 * Parameters   : none
 * Returns      : rf cal sector
*******************************************************************************/
uint32 user_rf_cal_sector_set(void)
{
    flash_size_map size_map = system_get_flash_size_map();
    uint32 rf_cal_sec = 0;

    switch (size_map) {
        case FLASH_SIZE_4M_MAP_256_256:
            rf_cal_sec = 128 - 5;
            break;

        case FLASH_SIZE_8M_MAP_512_512:
            rf_cal_sec = 256 - 5;
            break;

        case FLASH_SIZE_16M_MAP_512_512:
        case FLASH_SIZE_16M_MAP_1024_1024:
            rf_cal_sec = 512 - 5;
            break;

        case FLASH_SIZE_32M_MAP_512_512:
        case FLASH_SIZE_32M_MAP_1024_1024:
            rf_cal_sec = 1024 - 5;
            break;

        default:
            rf_cal_sec = 0;
            break;
    }

    return rf_cal_sec;
}



/*
 *参数是void *date是为了让任务在创建的时候可以传进来任意的参数
 * */
void LedControl(void *date)
{
  while(1)
  {
	  vTaskDelay(1000/portTICK_RATE_MS );//延时1S
//	  GPIO_OUTPUT_SET(5, 1-GPIO_INPUT_GET(5));//GOIO5
  }
  vTaskDelete(NULL);//删除这个任务
}

/*
 *参数是void *date是为了让任务在创建的时候可以传进来任意的参数
 * */
void LedControl2(void *date)
{
  while(1)
  {
	  vTaskDelay(1000/portTICK_RATE_MS );//延时1S
	  GPIO_OUTPUT_SET(2, 1-GPIO_INPUT_GET(2));//GPIO2
  }
  vTaskDelete(NULL);//删除这个任务
}



void UartDispose(void *data)
{
	long i =0;
	while(1)
	{
		vTaskDelay(10/portTICK_RATE_MS );//延时10ms
		if(Usart1ReadFlage == 1)//串口接受到数据
		{
			Usart1ReadFlage = 0;//清零

			if(Usart1ReadBuff[0] == 0xaa && Usart1ReadBuff[1] == 0x55 )
			{
				if(Usart1ReadBuff[2] == 0x03)
				{
					ResolveDataTest.Data_Table[0] = Usart1ReadBuff[3];
					ResolveDataTest.Data_Table[1] = Usart1ReadBuff[4];
					ResolveDataTest.Data_Table[2] = Usart1ReadBuff[5];
					ResolveDataTest.Data_Table[3] = Usart1ReadBuff[6];
					pwm_set_duty (ResolveDataTest.DatauInt, 0);//设置0通道的PWM高电平时间
					pwm_start();//启动
				}
			}

			for(i=0;i<Usart1ReadCntCopy;i++)
			{
				USART_SendData(UART0, Usart1ReadBuff[i]);//返回接收的数据
			}
		}
	}
	vTaskDelete(NULL);
}

//定义一个定时器用的回调函数
void os_timer_one_function(void *parg)
{
	os_printf("parg:%s\n", parg);
//	printf("parg:%s\n", parg);//打印一下传过来的参数
	GPIO_OUTPUT_SET(5, 1-GPIO_INPUT_GET(5));//GPIO5输出 反转


}




u32 cnt = 0;
int val = 1;
void hw_test_timer_cb(void)
{
	cnt++;
	if(cnt>10)//10ms改变一次PWM的高电平输出时间
	{
		cnt=0;
		pwm_set_duty (duty[1], 0);//设置0通道的PWM高电平时间
		pwm_start();//启动
		duty[1] = duty[1] + 5*val;//如果val是正数  每次进来累加5   如果是负数每次进来减5
		if(duty[1]>1000)//到达最大值,最亮
		{
			duty[1] = 1000;//设置一下最大值,不超过1000周期
			val=-1;//val改为负数
		}
		if(duty[1]<0)//到达最小值,灭
		{
			duty[1] = 0;
			val=1;//val改为正数
		}

	}
}


/******************************************************************************
 * FunctionName : user_init
 * Description  : entry of user application, init user function here
 * Parameters   : none
 * Returns      : none
*******************************************************************************/
void user_init(void)
{
	GPIO_OUTPUT_SET(5, 1);
	GPIO_OUTPUT_SET(2, 0);//让两个灯初始的状态一样,GOIO2是反接的,0的时候是亮
//	GPIO_OUTPUT_SET(5, 0);
	uart_init_new();
//	printf("SDK version:%s\n", system_get_sdk_version());
//	printf("Ai-Thinker Technology Co. Ltd.\r\n%s %s\r\n", __DATE__, __TIME__);
//	printf("Hello,World!\r\n");
//	xTaskCreate(LedControl, "LedControl", 1024, NULL, 11, NULL);
////	LedControl:函数地址;  "LedControl":这个任务的名字,一般用不到,调试的时候有名字好找问题
////	1024 字节   操作系统需要记录这个任务的信息嘛,所以需要指定用多大内存来记录任务的运行信息,这个参数先这样哈,其实操作系统有函数可以得到具体使用多少
////	NULL 就是void LedControl(void *date)  传给date的内容,咱现在不需要传什么,所以就是null
////	11  这个是优先级,任务也有优先级,先执行哪个后执行哪个,也可以打断,和中断差不多,这个是有最大的,其实最大多少就说明了可以创建多少个任务
////	NULL  最后一个参数是用来获取任务的一些信息的东西,咱现在不需要哈,用的时候再学
//	xTaskCreate(LedControl2, "LedControl2", 1024, NULL, 8, NULL);
	xTaskCreate(UartDispose, "UartDispose", 1024, NULL, 4, NULL);
//	os_timer_setfn(&os_timer_one,os_timer_one_function,"yang");//配置定时器          定时器结构体变量         回调函数                   传给回调函数的参数
//	os_timer_arm(&os_timer_one,3000,1);//使能定时器    os_timer_one--定时器变量    500--500ms进一次       1--循环进去
//
//	hw_timer_init(1);//循环进进入
//	hw_timer_set_func(hw_test_timer_cb);//设置回调函数
//	hw_timer_arm(1000);//延时..一会测试看看是延时多少


	pwm_init(1000, duty, 1, pin_info_list);//周期1000us   高电平时间100us   1 --就配置了一个管脚,因为数组是[1][3]   pin_info_list--数组
	pwm_start();//启动PWM
}

