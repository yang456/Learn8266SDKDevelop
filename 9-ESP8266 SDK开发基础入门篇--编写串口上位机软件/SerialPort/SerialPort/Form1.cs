﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SerialPort
{
    

    public partial class Form1 : Form
    {
        string PortNameCopy;//记录打开的是哪个串口
        byte[] UsartReadBuff = new byte[2048];//串口接收的数据
        int UsartReadCnt = 0;//串口接收到的数据个数

        public Form1()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //页面一开始加载的时候执行这个
            string[] ports = System.IO.Ports.SerialPort.GetPortNames();//获取电脑上可用串口号
            comboBox1.Items.AddRange(ports);//给comboBox1添加数据
            comboBox1.SelectedIndex = comboBox1.Items.Count > 0 ? 0 : -1;//如果里面有数据,显示第0个
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (button1.Text == "打开")//如果按钮显示的是打开
            {
                try//防止意外错误
                {
                    serialPort1.PortName = comboBox1.Text;//得到comboBox1显示的串口内容
                    serialPort1.BaudRate = Convert.ToInt32(comboBox2.Text);//得到comboBox2显示的波特率内容
                    serialPort1.Open();//打开串口
                    button1.Text = "关闭";//按钮显示关闭
                    PortNameCopy = serialPort1.PortName;
                }
                catch (Exception)
                {
                    MessageBox.Show("打开失败", "提示!");//对话框显示打开失败
                }
            }
            else//要关闭串口
            {
                try//预防串口有问题了,实际上已经关了
                {
                    serialPort1.Close();//关闭串口
                }
                catch (Exception)
                {
                }
                button1.Text = "打开";//按钮显示打开
            }
        }

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == 0x0219)//设备改变
            {
                if (m.WParam.ToInt32() == 0x8004)//usb串口拔出
                {
                    string[] ports = System.IO.Ports.SerialPort.GetPortNames();//重新获取串口
                    comboBox1.Items.Clear();
                    comboBox1.Items.AddRange(ports);
                    if (button1.Text == "关闭")//咱打开过一个串口
                    {
                        if (!serialPort1.IsOpen)//咱打开的那个关闭了,说明拔插的是咱打开的
                        {
                            button1.Text = "打开";
                            serialPort1.Dispose();//释放掉原先的串口资源
                            comboBox1.SelectedIndex = comboBox1.Items.Count > 0 ? 0 : -1;//显示获取的第一个串口号
                        }
                        else//热插拔不是咱打开的那个
                        {
                            comboBox1.Text = PortNameCopy;//默认显示的是咱打开的那个串口号
                        }
                    }
                    else//没有打开过
                    {
                        comboBox1.SelectedIndex = comboBox1.Items.Count > 0 ? 0 : -1;//显示获取的第一个串口号
                    }
                }
                else if (m.WParam.ToInt32() == 0x8000)//usb串口连接上
                {
                    string[] ports = System.IO.Ports.SerialPort.GetPortNames();//重新获取串口
                    comboBox1.Items.Clear();
                    comboBox1.Items.AddRange(ports);
                    if (button1.Text == "关闭")//咱打开过一个串口
                    {
                        comboBox1.Text = PortNameCopy;//默认显示的是咱打开的那个串口号
                    }
                    else
                    {
                        comboBox1.SelectedIndex = comboBox1.Items.Count > 0 ? 0 : -1;//显示获取的第一个串口号
                    }                          
                }
            }
            base.WndProc(ref m);
        }

        //串口接收到数据就会进入
        private void serialPort1_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            int len = serialPort1.BytesToRead;//获取可以读取的字节数
            if (len > 0)
            {
                byte[] recvBytes = new byte[len];//创建接收的数组
                serialPort1.Read(recvBytes, 0, len);//接收数据

                Invoke((new Action(() =>//显示字符串
                {
                    textBox1.AppendText("字符串:"+Encoding.Default.GetString(recvBytes)); //显示在文本框里面
                })));

                Invoke((new Action(() =>//显示16进制
                {
                    textBox1.AppendText("\r\n16进制:" + byteToHexStr(recvBytes) + "\r\n"); //显示在文本框里面
                })));

                for (int i = 0; i < len; i++)//拷贝数据到UsartReadBuff
                {
                    UsartReadBuff[i+ UsartReadCnt] = recvBytes[i];//从上次的地方接着填入数据
                }
                UsartReadCnt = UsartReadCnt + len;//记录上次的数据个数
                if (UsartReadCnt >= 3)//接收到可以处理的数据个数
                {
                    UsartReadCnt = 0;
                    if (UsartReadBuff[0] == 0xaa && UsartReadBuff[1] == 0x55)//判断数据
                    {
                        if (UsartReadBuff[2] == 0x01)//
                        {
                            Invoke((new Action(() =>
                            {
                                button3.Text = "熄灭";
                                label5.Text = "点亮";
                            })));
                        }
                        else if (UsartReadBuff[2] == 0x00)
                        {
                            Invoke((new Action(() =>
                            {
                                button3.Text = "点亮";
                                label5.Text = "熄灭";
                            })));
                        }
                    }
                }
            }
        }


        //按钮事件
        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                if (button3.Text == "点亮")
                {
                    byte[] sendbyte = new byte[3];
                    sendbyte[0] = 0xaa;
                    sendbyte[1] = 0x55;
                    sendbyte[2] = 0x01;
                    serialPort1.Write(sendbyte, 0, sendbyte.Length);
                }
                else if(button3.Text == "熄灭")
                {
                    byte[] sendbyte = new byte[3];
                    sendbyte[0] = 0xaa;
                    sendbyte[1] = 0x55;
                    sendbyte[2] = 0x00;
                    serialPort1.Write(sendbyte, 0, sendbyte.Length);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("发送失败,请检查串口", "提示!");
                try
                {
                    serialPort1.Close();
                }
                catch (Exception)
                {
                }
                button1.Text = "打开";
            }
        }

        /// <字节数组转16进制字符串>
        /// <param name="bytes"></param>
        /// <returns> String 16进制显示形式</returns>
        public static string byteToHexStr(byte[] bytes)
        {
            string returnStr = "";
            try
            {
                if (bytes != null)
                {
                    for (int i = 0; i < bytes.Length; i++)
                    {
                        returnStr += bytes[i].ToString("X2");
                        returnStr += " ";//两个16进制用空格隔开,方便看数据
                    }
                }
                return returnStr;
            }
            catch (Exception)
            {
                return returnStr;
            }
        }



        /// <字符串转16进制格式,不够自动前面补零>
        /// 
        /// </summary>
        /// <param name="hexString"></param>
        /// <returns></returns>
        private static byte[] strToToHexByte(String hexString)
        {
            int i;
            bool Flag = false;


            hexString = hexString.Replace(" ", "");//清除空格
            if ((hexString.Length % 2) != 0)
            {
                Flag = true;
            }
            if (Flag == true)
            {
                byte[] returnBytes = new byte[(hexString.Length + 1) / 2];

                try
                {
                    for (i = 0; i < (hexString.Length - 1) / 2; i++)
                    {
                        returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
                    }
                    returnBytes[returnBytes.Length - 1] = Convert.ToByte(hexString.Substring(hexString.Length - 1, 1).PadLeft(2, '0'), 16);

                }
                catch
                {
                    for (i = 0; i < returnBytes.Length; i++)
                    {
                        returnBytes[i] = 0;
                    }
                    MessageBox.Show("超过16进制范围A-F,已初始化为0", "提示");
                }
                return returnBytes;
            }
            else
            {
                byte[] returnBytes = new byte[(hexString.Length) / 2];
                try
                {
                    for (i = 0; i < returnBytes.Length; i++)
                    {
                        returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
                    }
                }
                catch
                {
                    for (i = 0; i < returnBytes.Length; i++)
                    {
                        returnBytes[i] = 0;
                    }
                    MessageBox.Show("超过16进制范围A-F,已初始化为0", "提示");
                }
                return returnBytes;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBox2.Text.ToString().Length>0)//发送文本框有数据
                {
                    serialPort1.Write(textBox2.Text.ToString());//发送
                }
            }
            catch (Exception)
            {
                MessageBox.Show("发送失败,请检查串口", "提示!");
                button1.Text = "打开";
            }
        }
    }
}


