/*
 * ESPRSSIF MIT License
 *
 * Copyright (c) 2015 <ESPRESSIF SYSTEMS (SHANGHAI) PTE LTD>
 *
 * Permission is hereby granted for use on ESPRESSIF SYSTEMS ESP8266 only, in which case,
 * it is free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include "esp_common.h"
#include "gpio.h"
#include "uart.h"


extern u8  Usart1ReadBuff[Usart1ReadLen];//接收数据的数组
extern u32 Usart1ReadCnt;//串口1接收到的数据个数
extern u32 Usart1ReadCntCopy;//串口1接收到的数据个数拷贝
extern u8  Usart1ReadFlage;//串口1接收到一条完整数据


/******************************************************************************
 * FunctionName : user_rf_cal_sector_set
 * Description  : SDK just reversed 4 sectors, used for rf init data and paramters.
 *                We add this function to force users to set rf cal sector, since
 *                we don't know which sector is free in user's application.
 *                sector map for last several sectors : ABCCC
 *                A : rf cal
 *                B : rf init data
 *                C : sdk parameters
 * Parameters   : none
 * Returns      : rf cal sector
*******************************************************************************/
uint32 user_rf_cal_sector_set(void)
{
    flash_size_map size_map = system_get_flash_size_map();
    uint32 rf_cal_sec = 0;

    switch (size_map) {
        case FLASH_SIZE_4M_MAP_256_256:
            rf_cal_sec = 128 - 5;
            break;

        case FLASH_SIZE_8M_MAP_512_512:
            rf_cal_sec = 256 - 5;
            break;

        case FLASH_SIZE_16M_MAP_512_512:
        case FLASH_SIZE_16M_MAP_1024_1024:
            rf_cal_sec = 512 - 5;
            break;

        case FLASH_SIZE_32M_MAP_512_512:
        case FLASH_SIZE_32M_MAP_1024_1024:
            rf_cal_sec = 1024 - 5;
            break;

        default:
            rf_cal_sec = 0;
            break;
    }

    return rf_cal_sec;
}

/*
 *参数是void *date是为了让任务在创建的时候可以传进来任意的参数
 * */
void LedControl(void *date)
{
  while(1)
  {
	  vTaskDelay(1000/portTICK_RATE_MS );//延时1S
	  GPIO_OUTPUT_SET(5, 1-GPIO_INPUT_GET(5));//GOIO5
  }
  vTaskDelete(NULL);//删除这个任务
}

/*
 *参数是void *date是为了让任务在创建的时候可以传进来任意的参数
 * */
void LedControl2(void *date)
{
  while(1)
  {
	  vTaskDelay(1000/portTICK_RATE_MS );//延时1S
	  GPIO_OUTPUT_SET(2, 1-GPIO_INPUT_GET(2));//GPIO2
  }
  vTaskDelete(NULL);//删除这个任务
}

/******************************************************************************
 * FunctionName : user_init
 * Description  : entry of user application, init user function here
 * Parameters   : none
 * Returns      : none
*******************************************************************************/
void user_init(void)
{
	GPIO_OUTPUT_SET(5, 1);
	GPIO_OUTPUT_SET(2, 0);//让两个灯初始的状态一样,GOIO2是反接的,0的时候是亮
//	GPIO_OUTPUT_SET(5, 0);

	uart_init_new();
//	printf("SDK version:%s\n", system_get_sdk_version());
//	printf("Ai-Thinker Technology Co. Ltd.\r\n%s %s\r\n", __DATE__, __TIME__);
//	printf("Hello,World!\r\n");

	xTaskCreate(LedControl, "LedControl", 1024, NULL, 11, NULL);
	//LedControl:函数地址;  "LedControl":这个任务的名字,一般用不到,调试的时候有名字好找问题
	//1024 字节   操作系统需要记录这个任务的信息嘛,所以需要指定用多大内存来记录任务的运行信息,这个参数先这样哈,其实操作系统有函数可以得到具体使用多少
	//NULL 就是void LedControl(void *date)  传给date的内容,咱现在不需要传什么,所以就是null
	//11  这个是优先级,任务也有优先级,先执行哪个后执行哪个,也可以打断,和中断差不多,这个是有最大的,其实最大多少就说明了可以创建多少个任务
	//NULL  最后一个参数是用来获取任务的一些信息的东西,咱现在不需要哈,用的时候再学

	xTaskCreate(LedControl2, "LedControl2", 1024, NULL, 8, NULL);
}



//void UartDispose(void *data)
//{
//	long i =0;
//	while(1)
//	{
//		vTaskDelay(1000/portTICK_RATE_MS );
//		if(Usart1ReadFlage == 1)
//		{
//			Usart1ReadFlage = 0;
//
//			for(i=0;i<Usart1ReadCntCopy;i++)
//			{
//				USART_SendData(UART0, Usart1ReadBuff[i]);
//			}
//		}
//		GPIO_OUTPUT_SET(5, 1-GPIO_INPUT_GET(5));
//	}
//	vTaskDelete(NULL);
//}



/*
 * for(i=0;i<1000;i++)
	{
		os_delay_us(200);
	}
	GPIO_OUTPUT_SET(4, 0);
	for(i=0;i<1000;i++)
	{
		os_delay_us(200);
	}
	GPIO_OUTPUT_SET(4, 1);

	for(i=0;i<1000;i++)
	{
		os_delay_us(200);
	}
	GPIO_OUTPUT_SET(4, 0);
 */

