﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace TCPClient
{
    public partial class Form1 : Form
    {
        private TcpClient myTcpClient = null;// TcpClient

        Thread ConnectThread;//连接线程

        string ipAddress;//记录ip地址
        int Port = 0;//端口号

        private NetworkStream networkstrem = null;//获取网络数据用
        private Thread ReceiveThread;//接收消息线程
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            getIPAddress();//刚才写的那个函数.获取电脑IP,并显示在下拉框
        }


        /// <获取本机 IP 地址>
        /// 
        /// </summary>
        /// <returns></returns>
        private void getIPAddress()
        {
            IPAddress[] hostipspool = Dns.GetHostAddresses("");//获取本机所以IP
            comboBox1.Items.Clear();//清除下拉框里面的内容
            foreach (IPAddress ipa in hostipspool)
            {
                if (ipa.AddressFamily == AddressFamily.InterNetwork)
                {
                    comboBox1.Items.Add(ipa.ToString());//下拉框加入IP数据
                    comboBox1.SelectedIndex = comboBox1.Items.Count > 0 ? 0 : -1;//显示第一个
                }
            }
        }


        private void ConnectMethod()
        {
            myTcpClient = new TcpClient();                      //实例化myTcpClient
            try
            {
                myTcpClient.Connect(ipAddress, Port);//连接服务器

                networkstrem = myTcpClient.GetStream();//获取数据流操作实例.(给的方法就是这个......)

                ReceiveThread = new Thread(ReceiveDataMethod);//启动接收数据任务
                ReceiveThread.Start();
 
                //连接上以后往下执行
                Invoke((new Action(() => 
                {
                    button1.Text = "断开";
                })));
            }
            catch (Exception)
            {
                //异常处理函数
                Invoke((new Action(() =>
                {
                    button1.Text = "连接";
                })));
                try { ReceiveThread.Abort(); }//销毁任务
                catch { }
                try { networkstrem.Dispose(); }//释放资源
                catch { }
                try { myTcpClient.Close(); }//关闭TCP
                catch { }
            }
        }



        /// <接收消息线程>
        /// 
        /// </summary>
        private void ReceiveDataMethod()
        {
            int RecvCnt = 0;
            byte[] recvBytes = new byte[1024];
            while (true)
            {
                try
                {
                    //检测服务器是主动断开
                    if ((myTcpClient.Client.Poll(20, SelectMode.SelectRead)) && (myTcpClient.Client.Available == 0))
                    {
                        myTcpClient.Close();//关闭以后,后面程序会引发异常
                    }

                    RecvCnt = networkstrem.Read(recvBytes, 0, recvBytes.Length);//获取数据

                    Invoke((new Action(() =>
                    {
                        //new ASCIIEncoding().GetString(recvBytes, 0, RecvCnt)//byte转化为字符串
                        textBox2.AppendText(new ASCIIEncoding().GetString(recvBytes, 0, RecvCnt));//追加显示
                    })));
                }
                catch (Exception ex)//连接以后关闭窗体,会进这个异常
                {
                    //异常处理函数
                    try
                    {//其实任务被干掉了,这些往下会提示说任务已经不在了
                        Invoke((new Action(() =>
                        {
                            button1.Text = "连接";
                        })));
                    }
                    catch
                    {
                    } 
                    
                    try { ReceiveThread.Abort(); }//销毁任务
                    catch { }
                    try { networkstrem.Dispose(); }//释放资源
                    catch { }
                    try { myTcpClient.Close(); }//关闭TCP
                    catch { }
                }
            }
        }



        //连接和断开按钮
        private void button1_Click(object sender, EventArgs e)
        {
            if (button1.Text == "连接")
            {
                ipAddress = comboBox1.Text.ToString();//获取IP地址
                Port = Convert.ToInt32(textBox1.Text.ToString());//获取端口号

                ConnectThread = new Thread(ConnectMethod);//创建任务
                ConnectThread.Start();//启动任务
            }
            else
            {
                try { myTcpClient.Close(); } catch { } //关闭连接
                Invoke((new Action(() =>
                {
                    button1.Text = "连接";
                })));
            }
        }

        private void comboBox1_DropDown(object sender, EventArgs e)
        {
            getIPAddress();//刚才写的那个函数
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            try { ReceiveThread.Abort(); }//销毁任务
            catch { }
            try { networkstrem.Dispose(); }//释放资源
            catch { }
            try { myTcpClient.Close(); }//关闭TCP
            catch { }
        }

        //发送数据
        private void button3_Click(object sender, EventArgs e)
        {
            if (!checkBox2.Checked)//字符发送
            {
                byte[] sendbyte = Encoding.Default.GetBytes(textBox3.Text.ToString());//获取发送的数据,转为byte
                if (sendbyte.Length > 0)
                {
                    try { networkstrem.Write(sendbyte, 0, sendbyte.Length); }//发送数据
                    catch (Exception) { MessageBox.Show("请检查连接", "提示!"); }
                }
                else
                {
                    MessageBox.Show("数据不能为空", "提示!");
                }
            }
            else//16形式进制发送
            {
                byte[] sendbyte = strToToHexByte(textBox3.Text.ToString());//字符串转16进制格式,不够自动前面补零
                if (sendbyte.Length > 0)
                {
                    try { networkstrem.Write(sendbyte, 0, sendbyte.Length); }
                    catch (Exception) { MessageBox.Show("请检查连接", "提示!"); }
                }
                else
                {
                    MessageBox.Show("数据不能为空", "提示!");
                }
            }
        }



        /// <字符串转16进制格式,不够自动前面补零>
        /// "0054FF"  ==>  16进制  0x00 0x54 0xFF
        /// </summary>
        /// <param name="hexString"></param>
        /// <returns></returns>
        private static byte[] strToToHexByte(String hexString)
        {
            int i;
            bool Flag = false;


            hexString = hexString.Replace(" ", "");//清除空格
            if ((hexString.Length % 2) != 0)
            {
                Flag = true;
            }
            if (Flag == true)
            {
                byte[] returnBytes = new byte[(hexString.Length + 1) / 2];

                try
                {
                    for (i = 0; i < (hexString.Length - 1) / 2; i++)
                    {
                        returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
                    }
                    returnBytes[returnBytes.Length - 1] = Convert.ToByte(hexString.Substring(hexString.Length - 1, 1).PadLeft(2, '0'), 16);

                }
                catch
                {
                    for (i = 0; i < returnBytes.Length; i++)
                    {
                        returnBytes[i] = 0;
                    }
                    MessageBox.Show("超过16进制范围A-F,已初始化为0", "提示");
                }
                return returnBytes;
            }
            else
            {
                byte[] returnBytes = new byte[(hexString.Length) / 2];
                try
                {
                    for (i = 0; i < returnBytes.Length; i++)
                    {
                        returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
                    }
                }
                catch
                {
                    for (i = 0; i < returnBytes.Length; i++)
                    {
                        returnBytes[i] = 0;
                    }
                    MessageBox.Show("超过16进制范围A-F,已初始化为0", "提示");
                }
                return returnBytes;
            }
        }

    }
}
